import React from "react";
import { Card } from "../Card/Card";



export class CardList extends React.Component{
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {items:[]}

    }
    componentDidMount() {
        fetch('/data/items.json')
          .then(res => res.json())
          .then((data) => 
          {
            const localOrder= localStorage.getItem('orders')
            if (localOrder){
                const orders = data.products.filter(({article})=>localOrder.includes(article))
                this.setState(prev=>({
                    ...prev,
                    orders,
                    items: data.products,
                  
                  }));
                  console.log(orders);
            } 
           else{
            this.setState(prev=>({
                ...prev,
                items: data.products,
              
              }));
             
           }
          
        })
        .catch(error=>console.log(error))
      }
    
    render()
    {
        return(
            <div className="card-сontainer">
                {this.state.items.map(item=>(
                    <Card
                    key={item.id}
                    article={item.article}
                    name={item.name}
                    color={item.color}
                    image={item.image}
                    price={item.price}
                    onAdd={this.props.onAdd}
                    />                    
                ))}
            </div>
        )
    }
}


