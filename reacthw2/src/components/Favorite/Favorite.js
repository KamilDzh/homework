import React, { Component } from "react";
import { Button } from "../Button/Button";
import { Modal } from "../Button/Modal";
export class Favorite extends Component {

    constructor(props) {
        super(props);
        this.state = {
            
            isModalVisible: false 
        }
        this.modalClick = this.modalClick.bind(this)
        this.modalClickOff = this.modalClick.bind(this)
    this.AddToFavorite= this.AddToFavorite.bind(this)
    }
    modalClick (){
        this.setState({ isModalVisible: true })
    }
    modalClickOff(){
        this.setState({ isModalVisible: false })
    }

    AddToFavorite() {
        fetch('/data/items.json')
          .then(res => res.json())
          .then((data) => 
          {
            const localOrder= localStorage.getItem('orders')
            if (localOrder){
                const orders = data.products.filter(({article})=>localOrder.includes(article))
                this.setState(prev=>({
                    ...prev,
                    orders,
                    items: data.products,
                  
                  }));
                  console.log(orders);
            } 
           else{
            this.setState(prev=>({
                ...prev,
                items: data.products,
                
              }));
             
           }
          
        })
        .catch(error=>console.log(error))
      }

    render() {

        return (
            <div className="star-item" onClick={this.modalClick} >
                <svg 
                    xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" width="26px"  strokeWidth="1.5" stroke="currentColor" >
                    <path strokeLinecap="round" strokeLinejoin="round" d="M11.48 3.499a.562.562 0 011.04 0l2.125 5.111a.563.563 0 00.475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 00-.182.557l1.285 5.385a.562.562 0 01-.84.61l-4.725-2.885a.563.563 0 00-.586 0L6.982 20.54a.562.562 0 01-.84-.61l1.285-5.386a.562.562 0 00-.182-.557l-4.204-3.602a.563.563 0 01.321-.988l5.518-.442a.563.563 0 00.475-.345L11.48 3.5z" />
                </svg>
                {this.state.isModalVisible && (<Modal
                    header=" дотати товар до обранного"
                    text="Вы точно хочете додати товар до обранного?"
                    backgroundColor="green"
                    onClose={() => { this.setState({ isModalVisible: false }) }}
                    actions={
                        <>
                            <Button className="btn1" text="Ok" handleClick={() =>(this.AddToFavorite)}/>
                            <Button className="btn2" text="Cancel" onClick={this.modalClickOff}
                             />
                        </>}
                />)}
            </div>
        )
    };

}
