import React from "react";
import { Button } from "../Button/Button";
import { Modal } from "../Button/Modal";
import { Favorite } from "../Favorite/Favorite";

export class Card extends React.Component {
    state = { isModalVisible: false }
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            orders: [],
        };
        this.addToOrder = this.addToOrder.bind(this)
     
    }
       
    
    render() {
        return (
            <div className="card">
                <li key={this.props.id}>
                    <img src={this.props.image} alt={this.props.name} />
                    <h3>{this.props.name}</h3>
                    <span>{this.props.price} грн</span>
                    <p>{this.props.color}</p>
                    <div className="AddBtn">
                        <Favorite/>
                        <Button className="modal_btn" text="add to card"
                            backgroundColor="green"
                            handleClick={(e) => {
                                this.setState({ isModalVisible: true })
                            }} />
                    </div>
                    {this.state.isModalVisible && (<Modal
                        header="Додання товару в кошик"
                        text="Ви хочете додати цей товар в кошик?"
                        backgroundColor="green"
                        onClose={() => { this.setState({ isModalVisible: false }) }}
                        actions={
                            <>
                                <Button className="btn1" text="Ok" handleClick={() => (this.addToOrder(this.props.article) ,this.setState({ isModalVisible: false })) } />
                                <Button className="btn2" text="Cancel" handleClick={(e) => {
                                    this.setState({ isModalVisible: false })
                                }} />
                            </>}
                    />)}
                </li>
            </div>
        );
    }
    addToOrder(article) {
        const localOrder = (localStorage.getItem('orders'))
        if (localOrder) {
            this.state.orders = JSON.parse(localOrder);
        }
        // this.state.orders.push(article
        this.setState(prevState => {
            const newOrders = [...prevState.orders]
            newOrders.push(article)
            localStorage.setItem('orders', JSON.stringify(newOrders));
            return {
                ...prevState, orders: newOrders
            };

        })



    }
}

