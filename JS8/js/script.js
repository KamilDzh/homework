// 1)
const paragraphContainer = document.querySelectorAll('p');
paragraphContainer.forEach((p) => (p.style.background = '#ff0000'));

//=================================================================

// 2)
const optionListEl = document.getElementById('optionsList');
console.log(optionListEl);

console.log(optionListEl.parentElement);

optionListEl.childNodes.forEach((e) => console.log(e.nodeType));
optionListEl.childNodes.forEach((e) => console.log(e.nodeName));

//=================================================================

// 3)
const testParagraph = document.querySelector('#testParagraph');
testParagraph.textContent = 'This is a paragraph';

let myMainHeader = document.querySelectorAll('.main-header li');
console.log(myMainHeader);
myMainHeader.forEach(item => {
  item.className = 'nav-item';
  console.log(item);
});

//=================================================================

// 5) 
let mySections = document.querySelectorAll('.section-title');
mySections.forEach(item => {
  item.classList.remove('section-title');
});
console.log(mySections);

//=================================================================

//task 6
const sectionTitleContainer = document.querySelectorAll('.section-title');
sectionTitleContainer.forEach((element) => element.classList?.remove('section-title'));
console.log(sectionTitleContainer);


//=================================================================
// Опишіть своїми словами що таке Document Object Model (DOM):

// DOM - це модель для відображення структури на сторінці нашого сайту

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?:

// innerHTML виводить html елементів разом із текстом
// innerText виводіть тільки текст елементу

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?:
// Способи: getElementById () , getElementsByName (), getElementsByTagName (), getElementsByClassName ()
// але найкращими на мою думку є querySelector та querySelectorAll. Це нові інструменти які краще за вже застарілі 