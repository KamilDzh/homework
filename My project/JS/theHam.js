const smallIconContainer = document.querySelector('.small-icons-container');
const smallIconItems = document.querySelectorAll('.small-icons-item');

const swiperSlideContainer = document.querySelectorAll('.swiper-slide');

smallIconItems[0].classList.add('swiper-icon-active');

smallIconContainer.addEventListener('click', swiperContentDisplay);

const swiperSlideFirstItem = document.querySelector('.swiper-slide');
const smallIconItem = document.querySelector('.small-icons-item');

if (smallIconItem.dataset.article === swiperSlideFirstItem.dataset.article) {
  swiperSlideFirstItem.classList.add('swiper-slide-active');
}

function swiperContentDisplay(e) {
  const selectedPicture = e.target.closest('img');
  if (!selectedPicture) {
    return;
  }
  const activeSwiperSlide = document.querySelector('.swiper-icon-active');

  if (activeSwiperSlide) {
    activeSwiperSlide.classList.remove('swiper-icon-active');
  }
  selectedPicture.classList.add('swiper-icon-active');

  swiperSlideContainer.forEach((item) => {
    item.classList.remove('swiper-slide-active');
    if (selectedPicture.dataset.article === item.dataset.article) {
      item.classList.add('swiper-slide-active');
    }
  });
}
const dynamicArrowsContainer = document.querySelector('.dynamic-buttons-wrapper');

const rightArrow = document.querySelector('.swiper-button-right');
const leftArrow = document.querySelector('.swiper-button-left');

dynamicArrowsContainer.addEventListener('click', dynamicArrows);

function dynamicArrows(e) {
  const arrow = e.target.closest('div');

  for (let index = 0; index <= smallIconItems.length; index++) {
    const elem = smallIconItems[index];

    if (arrow.contains(leftArrow) && elem.classList.contains('swiper-icon-active')) {
      if (index === 0) {
        index = smallIconItems.length;
      }
      elem.classList.remove('swiper-icon-active');
      smallIconItems[index - 1].classList.add('swiper-icon-active');
      break;
    }

    if (smallIconItems.length - 1 === index) {
      index = -1;
    }
    if (arrow.contains(rightArrow) && elem.classList.contains('swiper-icon-active')) {
      elem.classList.remove('swiper-icon-active');
      smallIconItems[index + 1].classList.add('swiper-icon-active');
      break;
    }
  }

  swiperSlideContainer.forEach((item) => {
    item.classList.remove('swiper-slide-active');
    smallIconItems.forEach((elem) => {
      if (elem.classList.contains('swiper-icon-active') && elem.dataset.article === item.dataset.article) {
        item.classList.add('swiper-slide-active');
      }
    });
  });
}
