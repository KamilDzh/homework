const imgSortArr = [
{
    category: 'Graphic Design',
    images: [
      './img/our-amazing-work/graphic-design/graphic-design1.jpg',
      './img/our-amazing-work/graphic-design/graphic-design2.jpg',
      './img/our-amazing-work/graphic-design/graphic-design3.jpg',
      './img/our-amazing-work/graphic-design/graphic-design4.jpg',
      './img/our-amazing-work/graphic-design/graphic-design5.jpg',
      './img/our-amazing-work/graphic-design/graphic-design6.jpg',
      './img/our-amazing-work/graphic-design/graphic-design7.jpg',
      './img/our-amazing-work/graphic-design/graphic-design8.jpg',
      './img/our-amazing-work/graphic-design/graphic-design9.jpg',
      './img/our-amazing-work/graphic-design/graphic-design10.jpg',
      './img/our-amazing-work/graphic-design/graphic-design11.jpg',
      './img/our-amazing-work/graphic-design/graphic-design12.jpg',
    ],
  },
  {
   
    category: 'Web Design',
    images: [
      './img/our-amazing-work/web-design/web-design1.jpg',
      './img/our-amazing-work/web-design/web-design2.jpg',
      './img/our-amazing-work/web-design/web-design3.jpg',
      './img/our-amazing-work/web-design/web-design4.jpg',
      './img/our-amazing-work/web-design/web-design5.jpg',
      './img/our-amazing-work/web-design/web-design6.jpg',
      './img/our-amazing-work/web-design/web-design7.jpg',
    ],
  },
  {
    category: 'Landing Pages',
    images: [
      './img/our-amazing-work/landing-page/landing-page1.jpg',
      './img/our-amazing-work/landing-page/landing-page2.jpg',
      './img/our-amazing-work/landing-page/landing-page3.jpg',
      './img/our-amazing-work/landing-page/landing-page4.jpg',
      './img/our-amazing-work/landing-page/landing-page5.jpg',
      './img/our-amazing-work/landing-page/landing-page6.jpg',
      './img/our-amazing-work/landing-page/landing-page7.jpg',
    ],
  },
  {
    category: 'Wordpress',
    images: [
      './img/our-amazing-work/wordpress/wordpress1.jpg',
      './img/our-amazing-work/wordpress/wordpress2.jpg',
      './img/our-amazing-work/wordpress/wordpress3.jpg',
      './img/our-amazing-work/wordpress/wordpress4.jpg',
      './img/our-amazing-work/wordpress/wordpress5.jpg',
      './img/our-amazing-work/wordpress/wordpress6.jpg',
      './img/our-amazing-work/wordpress/wordpress7.jpg',
      './img/our-amazing-work/wordpress/wordpress8.jpg',
      './img/our-amazing-work/wordpress/wordpress9.jpg',
      './img/our-amazing-work/wordpress/wordpress10.jpg',
    ],
  },
];
const imgArrayContainer = document.querySelector('.our-work-img-container');

const ourWorkTabs = document.querySelector('.our-work-tabs');
ourWorkTabs.addEventListener('click', sortCategoryBy);

function getSortedImagesByCategory(category) {
  let count = 0;
  let sortedImagesByCategory = [];
  imgSortArr.forEach((obj) => {
    if (category === obj.category) {
      sortedImagesByCategory = obj.images.map((imageSrc) => {
        return `
        <div class="flip-card" data-img-index=${count++}>
      <div class="flip-card-inner">
        <div class="flip-card-front">
        <img src=${imageSrc} data-category="${obj.category}" alt="Our work example"/>
        </div>
        <div class="flip-card-back">
          <div class="flip-card-back-img-container"> 
            <img src="./img/our-amazing-work/icons/Group-5.svg"/>
            <img src="./img/our-amazing-work/icons/Group-6.svg"/>
          </div>
          <p>CREATIVE DESIGN</p> 
          <h1>${obj.category}</h1> 
        </div>
      </div>
    </div>
      `;
      });
    }
  });
  return sortedImagesByCategory;
}

function sortCategoryBy(e) {
  const li = e.target.closest('li');
  if (!li) {
    return;
  }
  const activeSortItem = document.querySelector('.active-sort-item');

  if (activeSortItem) {
    activeSortItem.classList.remove('active-sort-item');
  }

  loadMoreBtn.classList.remove('load-more-btn-hidden');
  li.classList.add('active-sort-item');
  imgArrayContainer.innerHTML = '';

  if (li.dataset.category === 'all') {
    const allImages = getAllImages();
    const cutImagesArr = allImages.filter((image, idx) => idx < 12);
    imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));
  } else {
    const sortedArr = getSortedImagesByCategory(li.dataset.category);

    if (sortedArr.length <= 12) {
      loadMoreBtn.classList.add('load-more-btn-hidden');
    }
    imgArrayContainer.insertAdjacentHTML('afterbegin', sortedArr.join(''));
  }
}

function getAllImages() {
  let count = 0;
  const allImages = imgSortArr.map((obj) => {
    return obj.images.map((imageSrc) => {
      return `
      <div class="flip-card" data-img-index=${count++}>
      <div class="flip-card-inner">
        <div class="flip-card-front">
        <img src=${imageSrc} data-category="${obj.category}" alt="Our work example"/>
        </div>
        <div class="flip-card-back">
          <div class="flip-card-back-img-container"> 
            <img src="./img/our-amazing-work/icons/Group-5.svg"/>
            <img src="./img/our-amazing-work/icons/Group-6.svg"/>
          </div>
          <p>CREATIVE DESIGN</p> 
          <h1>${obj.category}</h1> 
        </div>
      </div>
    </div>
      `;
    });
  });
  const arr = [];
  allImages.forEach((elem) => {
    arr.push(...elem);
  });
  return arr;
}
let allImages = getAllImages();
const activeAllSortItem = document.querySelector('.sort-items');

if (activeAllSortItem.dataset.category === 'all') {
  allImages = getAllImages();
  const cutImagesArr = allImages.filter((image, idx) => idx < 12);
  imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));
}

activeAllSortItem.classList.add('active-sort-item');
const loadMoreBtn = document.querySelector('.load-more-btn');
loadMoreBtn.addEventListener('click', addMoreImages);

function addMoreImages(e) {
  const loadMoreBtn = document.querySelector('.load-more-btn');
  loadMoreBtn.innerHTML = '<i class="fa load-more-btn-icon fa-spinner fa-spin"></i> <span>Loading</span>';
  const ourWorkImgContainer = document.querySelector('.our-work-img-container');
  const ourWorkImgLastChild = ourWorkImgContainer.lastElementChild;
  setTimeout(() => {
    if (ourWorkImgLastChild.dataset.imgIndex === '23') {
      const thirdArr = allImages.slice(24, 36);
      imgArrayContainer.insertAdjacentHTML('beforeend', thirdArr.join(''));
      loadMoreBtn.innerHTML = `<img class="load-more-btn-icon" src="./img/our-amazing-work/icons/Forma 1.svg" alt="plus" />
      <span>LOAD MORE</span>`;
      loadMoreBtn.classList.add('load-more-btn-hidden');
    }
    if (ourWorkImgLastChild.dataset.imgIndex === '11') {
      const secondArr = allImages.slice(12, 24);
      imgArrayContainer.insertAdjacentHTML('beforeend', secondArr.join(''));
      loadMoreBtn.innerHTML = `<img class="load-more-btn-icon" src="./img/our-amazing-work/icons/Forma 1.svg" alt="plus" />
      <span>LOAD MORE</span>`;
    }
  }, 1000);
}
