import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Basket from './components/Pages/Basket/Basket';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Favorites from './components/Pages/Favorites/Favorites';
import Home from './components/Home/Home';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App/>,
    errorElement: <div>Not Found</div>,
    children:[
      {
        path: '/',
        element : <Home/>,
      },
      {
        path: "basket",
        element: <Basket/>,
      },
      {
        path: "favorite",
        element: <Favorites/>,
      },
    ]
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
