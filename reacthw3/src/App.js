import './App.scss';
import  Header  from './components/Header/Header';
import { useState } from 'react';
import { Outlet } from 'react-router-dom';
import { useEffect } from 'react';


function App(){
  const [items, setItems] = useState([]);
  const [orders, setOrders] = useState([]);
  const [favorites, setFavorites] = useState([])
  useEffect(() => {
    fetch("/data/items.json")
      .then((res) => res.json())
      .then((data) => {
        const localOrder = localStorage.getItem("orders");
        const localFavorites = localStorage.getItem("favorites")
        if (localOrder) {
          const orders = data.products.filter(({ article }) =>
            localOrder.includes(article)
          );
          setOrders(orders);
        }
        if (localFavorites) {
          const favorites = data.products.filter(({ article }) =>
          localFavorites.includes(article)
          );
          setFavorites(favorites);
        }
        setItems(data.products);
        console.log(localFavorites);
      })
      .catch((error) => console.log(error));
  }, []);

  return(
    <div className="App">
      <Header/>
     <Outlet context={{orders,setOrders, items, favorites, setFavorites }}/>
    </div>
  )
}
  

 



export default App;
