
import React, { useCallback } from 'react';

const Modal = (props) => {
  const handleClose = useCallback(() => {
    props.onClose();
  }, [props]);

  return (
    <div
      className="wrapper"
      onClick={handleClose}
    >
      <div
        className="modal"
        style={{ backgroundColor: props.backgroundColor }}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <h2 className="modal_header">{props.header}</h2>
        <div
          className="modal_closeWindow"
          onClick={handleClose}
        >
          X
        </div>
        <p className="modal_text">{props.text}</p>
        <div>{props.actions}</div>
      </div>
    </div>
  );
};

export default Modal;