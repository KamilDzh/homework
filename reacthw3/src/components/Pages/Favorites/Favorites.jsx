
import React from 'react'
import { useOutletContext } from 'react-router-dom'
import CardList from '../../CardList'

function Favorites() {
    const {favorites} = useOutletContext()
  return (
    <CardList items={favorites}/>
  )
}

export default Favorites