
import React from 'react'
import { useOutletContext } from 'react-router-dom'
import CardList from '../../CardList'





function Basket() {


  const { orders } = useOutletContext()
  return (

    <CardList items={orders} />

  )
}

export default Basket