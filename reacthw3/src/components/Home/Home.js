import React from 'react'
import { useOutletContext } from 'react-router-dom'
import CardList from '../CardList';

function Home() {
    const context = useOutletContext();
  return (
  <CardList items={context.items}/>
  )
}
 

export default Home