import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import './Header.scss'

export const Header = () => {
  const [cart, setCart] = useState([]);
  const [favorite,setFavorite] = useState([]);

  useEffect(() => {
    CartLength();
    FavoriteLength(); // change here
  }, []);

  const CartLength = () => {
    const cartsList = JSON.parse(localStorage.getItem("orders"));
    if (cartsList) {
      setCart(cartsList);
    }
    
  };
  const FavoriteLength = () => {
    const favoriteList = JSON.parse(localStorage.getItem("favorites"));
    if (favoriteList) {
      setFavorite(favoriteList);
    }
  };
  return (
    <div className="header-container">
      <NavLink to ="/" className={({isActive})=>
      isActive ? 'active-link' : undefined
    }><h1>Lab zone</h1></NavLink >

      <div className="svg-container">
        <NavLink to="/basket" className={({isActive})=>
      isActive ? 'active-link' : undefined
    }><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" viewBox="0 0 16 16"> <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" /> </svg></NavLink>
        <span>{cart.length}</span>
        <div> <NavLink  to ="/favorite" className={({isActive})=>
      isActive ? 'active-link' : undefined
    } ><svg
          xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" width="22.5px" strokeWidth="1.5" stroke="currentColor" >
          <path strokeLinecap="round" strokeLinejoin="round" d="M11.48 3.499a.562.562 0 011.04 0l2.125 5.111a.563.563 0 00.475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 00-.182.557l1.285 5.385a.562.562 0 01-.84.61l-4.725-2.885a.563.563 0 00-.586 0L6.982 20.54a.562.562 0 01-.84-.61l1.285-5.386a.562.562 0 00-.182-.557l-4.204-3.602a.563.563 0 01.321-.988l5.518-.442a.563.563 0 00.475-.345L11.48 3.5z" />
        </svg></NavLink >
        <span>{favorite.length}</span>
        </div>
      </div>
    </div>

  );

}


export default Header 