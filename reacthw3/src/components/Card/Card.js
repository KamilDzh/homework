import React, { useState, useEffect } from "react";
import Button from "../Button/Button";
import Modal from "../Button/Modal";
import Favorite from "../Favorite/Favorite";

function Card(props) {
  const [isModalVisible, setModalVisible] = useState(false);
  const [showConfirmationModal, setShowConfirmationModal] = useState(false);
  const [orders, setOrders] = useState([]);
  const [isButtonClicked, setButtonClicked] = useState(false);

  function addToOrder(article) {
    const localOrder = localStorage.getItem("orders");
    let newOrders = [];
    if (localOrder) {
      newOrders = JSON.parse(localOrder);
    }
    console.log("before filtering:", newOrders);
    newOrders.push(article);
    setOrders(newOrders);
    localStorage.setItem("orders", JSON.stringify(newOrders));
  }

  function deleteFromOrder() {
    const localOrder = localStorage.getItem("orders");
    let newOrders = [];
    if (localOrder) {
      newOrders = JSON.parse(localOrder);
    }

    newOrders = newOrders.filter((order) => order.id !== props.id);
    console.log("after filtering:", newOrders);
    setOrders(newOrders);
    localStorage.setItem("orders", JSON.stringify(newOrders));
    setShowConfirmationModal(true);
  }

  function handleButtonClick() {
    setButtonClicked(true);
  }

  function handleDeleteButtonClick() {
    deleteFromOrder();
    setModalVisible(false);
    setButtonClicked(false);
  }

  function handleConfirmationModalClose() {
    setShowConfirmationModal(false);
    setButtonClicked(false);
  }

  function handleConfirmationModalOk() {
    handleConfirmationModalClose();
  }

  function handleModalClose() {
    setModalVisible(false);
    setButtonClicked(false);
  }

  function handleModalOk() {
    addToOrder(props.article);
    setModalVisible(false);
    setButtonClicked(false);
  }

  useEffect(() => {
    const isCartPage = window.location.pathname === "/cart";
    if (isCartPage) {
      setModalVisible(false);
    }
  }, []);

  return (
    <div className="card">
      <li key={props.id}>
        <img src={props.image} alt={props.name} />
        <h3>{props.name}</h3>
        <span>{props.price} грн</span>
        <p>{props.color}</p>
        <div className="AddBtn">
          <Favorite article={props.article} />
          <Button
            className="modal_btn"
            text="add to card"
            backgroundColor="green"
            handleClick={handleButtonClick}
          />
          <Button
            className="modal_btn"
            text="delete"
            backgroundColor="red"
            handleClick={() => setModalVisible(true)}
          />
        </div>
        {isButtonClicked && (
          <Modal
            header="Додання товару в кошик"
            text="Ви хочете додати цей товар в кошик?"
            backgroundColor="green"
            onClose={handleModalClose}
            actions={
              <>
                <Button className="btn1" text="Ok" handleClick={handleModalOk} />
                <Button
                  className="btn2"
                  text="Cancel"
                  handleClick={handleModalClose}
                />
              </>
            }
          />
        )}
        {isModalVisible && (
          <Modal
            header="Видалення товару з кошика"
            text="Ви впевнені, що хочете видалити цей товар з кошика?"
            backgroundColor="red"
            onClose={handleModalClose}
            actions={
              <>
                <Button
                  className="btn1"
                  text="Ok"
                  handleClick={handleDeleteButtonClick}
                />
                <Button
                  className="btn2"
                  text="Cancel"
                  handleClick={handleModalClose}
                />
              </>
            }
          />
        )}
        {showConfirmationModal && (
          <Modal
            header="Видалення товару з кошика"
            text="Товар успішно видалено з кошика."
            backgroundColor="green"
            onClose={handleConfirmationModalClose}
            actions={
              <Button
                className="btn1"
                text="Ok"
                handleClick={handleConfirmationModalOk}
              />
            }
          />
        )}
      </li>
    </div>
  );
}

export default Card;