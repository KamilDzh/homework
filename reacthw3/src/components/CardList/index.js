import Card from '../Card/Card';

function CardList({ items }) {
  return (
    <div className="card-сontainer">
      {items.map((item) => (
        <Card
          key={item.id}
          article={item.article}
          name={item.name}
          color={item.color}
          image={item.image}
          price={item.price}

        />
      ))}
    </div>
  );
}

export default CardList;



