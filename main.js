/* 
  Теоретичні питання

Які існують типи даних у Javascript?

number & BigInt & Boolean & String & null & undefined & object

У чому різниця між == і ===?

"==" - прирівнює  значення без суворого  приведення типів
"===" - прирівнює значення тільки з однаковим типом даних

Що таке оператор?

Оператор проводить операції з операндами.
Оператори бувають унарні, бінарні та тернарні.
приклади операторів: + - * / % > < => =< 

*/
let userName = prompt("Enter your name");
while (!userName) {
    userName = prompt("Enter your name", userName);
}

let userAge = prompt("Enter your age");
while (Number.isNaN(+userAge) || userAge == "" || userAge == null) {
    userAge = +prompt("Enter your age", userAge);
}

if (userAge < 18) {
    alert(`You are not allowed to visit this website`);
} else if (userAge >= 18 && userAge <= 22) {
    let answer = confirm(`Are you sure you want to contine?`);
    if (answer) {
        alert(`Welcome, ${userName}`);
    } else {
        alert(`You are not allowed to visit this website`);
    }
} else {
    alert(`Welcome, ${userName}.`);
}