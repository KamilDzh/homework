const eyeEl = document.querySelectorAll('.icon-password');
const input = document.querySelectorAll('input');
const wrapper = document.querySelectorAll('.input-wrapper');
const submitBtn = document.querySelector('.btn');
const errorMessage = document.querySelector('.error');

eyeEl.forEach((eye) => {
  eye.addEventListener('click', (e) => {
    const input = e.currentTarget.previousSibling.previousSibling;

    if (input.type === 'password') {
      input.type = 'text';
      eye.classList.replace('fa-eye', 'fa-eye-slash');
    } else {
      input.type = 'password';
      eye.classList.replace('fa-eye-slash', 'fa-eye');
    }
  });
});

submitBtn.addEventListener('click', (e) => {
  e.preventDefault();
 if (input[0].value === input[1].value) {
   alert("You are welcome")
 }else{
  errorMessage.innerText = 'Нужно ввести правильное значения';
  errorMessage.style.color = 'red';
 }
})