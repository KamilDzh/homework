/*
1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

1. setTimeout дозволяє викликати функцію один раз через певний проміжок часу.
   setInterval дозволяє викликати функцію регулярно, повторюючи виклик через певний проміжок часу.
2. Функція буде намагатися виконати її якнайшвидше, але після основного коду
3. Щоб  запуск інтервалу не відбулись знову

*/


const startBtn = document.querySelector('.start-button')
const EndBtn = document.querySelector('.end-button')
const img = document.querySelectorAll(`.image-to-show`)
let temp;
let index = 0;
console.log(EndBtn);
clearInterval(temp);
temp = setInterval(ChangeImages, 3000);


function ChangeImages() {
    img[index === 0 ? img.length - 1 : index - 1].classList.remove("HideImage");
    img[index].classList.add('HideImage');
    index++;
    if (index >= img.length) {
        index = 0;
    }
}

EndBtn.addEventListener('click', () => {
    clearInterval(temp);
});


startBtn.addEventListener('click', () => {
    clearInterval(temp);
    temp = setInterval(ChangeImages, 3000);
});
