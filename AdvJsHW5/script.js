
async function GetUsersPost() {
    try {
        const GetUsers = await fetch('https://ajax.test-danit.com/api/json/users')
        const users = await GetUsers.json()
        console.log(users);


        const GetPosts = await fetch('https://ajax.test-danit.com/api/json/posts')
        const posts = await GetPosts.json()
        console.log(posts);
        users.forEach((user) => {
            posts.forEach((post) => {
                if (user.id === post.userId) {
                    const card = new Card(post.title, post.body, user.name, user.email, post.id)
                    card.render();
                }
            })
        })
        
    } catch (e) {
        console.log(e);
    }
}



class Card {
    constructor(title, body, name, email, id) {
        this.title = title;
        this.body = body;
        this.name = name;
        this.email = email;
        this.id = id;
    }
    render() {
        const deleteBtn = document.createElement('button')
        const divEl = document.createElement('div');
        const nameEl = document.createElement('div');
        const emailEl = document.createElement('p');
        const titleEl = document.createElement('div');
        const textEl = document.createElement('p'); 
        divEl.classList = 'card';
        titleEl.classList = 'title';
        deleteBtn.classList = 'btn-delete'
        textEl.classList = 'text'
        titleEl.innerText = this.title;
        textEl.innerText = this.body;
        nameEl.innerText = this.name;
        emailEl.innerText = this.email;
        deleteBtn.innerText = 'X' 
        divEl.append(deleteBtn);
        divEl.append(nameEl);
        divEl.append(emailEl);
        divEl.append(titleEl);
        divEl.append(textEl);
        document.body.append(divEl);
        const id = this.postId;
        deleteBtn.addEventListener('click', () => {
            (async() => {
                const result = await fetch (`https://ajax.test-danit.com/api/json/posts/${id}`, {
                    method: 'DELETE',
                }).then(response => {
                    console.log(response);
                    divEl.remove(id);
                });
            })();           
        });

    }
}


GetUsersPost();

