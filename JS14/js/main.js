const switchBtn = document.getElementById('switchBtn');
const theme = window.localStorage.getItem('theme');

if (theme === 'dark-theme') document.body.classList.add('dark-theme');

switchBtn.addEventListener('click', () => {
  document.body.classList.toggle('dark-theme');
  if (theme === 'dark-theme') {
    window.localStorage.setItem('theme', 'light-theme');
  } else window.localStorage.setItem('theme', 'dark-theme');
});

localStorage.getItem('theme');
localStorage.setItem('theme', 'dark-theme');

