const burgerSvg = document.querySelector(".burger-menu")
const burgerContainer = document.querySelector(".burger-menu-container")

burgerSvg.addEventListener("click", ()=> {
    burgerSvg.classList.toggle('active')
    burgerContainer.classList.toggle('active')
})

