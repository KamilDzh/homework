const buildFolder = `./dist`;
const srcFolder = `./src`;


export const path = {
    build : {
        css:  `${buildFolder}/css/`,
        html: `${buildFolder}/`,
        files: `${buildFolder}/files/`,
        js: `${buildFolder}/js/`,
        images: `${buildFolder}/img/`
    },
    src: {
        scss: `${srcFolder}/scss/style.scss`,
        files: `${srcFolder}/files/**/*.*`,
        html: `${srcFolder}/*.html`,
        js: `${srcFolder}/js/**/*`,
        images: `${srcFolder}/img/**/*`
    },
    watch: {
        scss: `${srcFolder}/scss/**/*.scss`,
        html: `${srcFolder}/**/*.html`,
        files: `${srcFolder}/files/**/*.*`,
        js: `${srcFolder}/js/**/*.js`,
        images: `${srcFolder}/img/**/*`
    },
    clean: buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    // rootFolder: rootFolder,
}