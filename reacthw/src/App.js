import './App.css';
import { Button } from './Button.js';
import { Modal } from './Modal';
import React from 'react';

class App extends React.Component {
  state = { isModalVisibleFirst: false, isModalVisibleSecond: false }
  render() {
    return (
      <div className="App">
        <Button className = "modal_btn" text="first modal window" 
        backgroundColor="red"
        handleClick={(e) => {
          this.setState({isModalVisibleFirst: true})
        }} />
        <Button className = "modal_btn" text="second modal window"
        backgroundColor="purple"
         handleClick={(e) => {
          this.setState({isModalVisibleSecond: true})
        }} />
        
        {this.state.isModalVisibleFirst && (<Modal 
        header="Do you want to delete this file?" 
        text="Once you delete this file, it won’t be possible to undo this action. 
        Are you sure you want to delete it?"
        backgroundColor="red"
        onClose={() => {this.setState({isModalVisibleFirst: false})}}
        actions={
          <>
          <Button className="btn1" text= "Ok" />
          <Button className="btn2" text= "Cancel"  handleClick={(e) => {
          this.setState({isModalVisibleFirst: false})
        }}/>
          </>}
         />)}
             {this.state.isModalVisibleSecond && (<Modal 
        header="Should I open this file?" 
        text="If you delete this file, it will not be returned, are you sure?"
        backgroundColor = "purple"
        onClose={() => {this.setState({isModalVisibleSecond: false})}}
        actions={
          <>
          <Button className="btn1" text= "Yes"  />
          <Button className="btn2" text= "No" handleClick={(e) => {
          this.setState({isModalVisibleSecond: false})
        }} />
          </>}
         />)}
      </div>
    );
  }
 
}

export default App;
