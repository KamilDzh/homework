/*
1. Метод об'єкта це функція яка належить об'єкту
2. Значення властивості може бути довільного типу. Навіть змінною
3. Це означає, що ми можемо його використовувати його явно, а присвоєнні дані будуть у виді посилання на них
*/

let firstName = prompt(`What is your name?`).toLowerCase();
let lastName = prompt(`What is your last name?`).toLowerCase();
    function createNewUser(firstName, lastName) {
        return {
            firstName: firstName,
            lastName: lastName,
        };
    };
const newUser = createNewUser(firstName, lastName);
console.log(newUser.firstName[0] + newUser.lastName);