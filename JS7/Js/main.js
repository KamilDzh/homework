////////////////////////////////////////////////
/*
1. Метод forEach() виконує функцію callback один раз для кожного елемента, що знаходиться в масиві в порядку зростання
2. Щоб очистити масив від елементів, потрібно встановити його довжину на нуль. Ця операція видаляє всі елементи масиву, і навіть їх значення. Приклад: arr.length = 0
3. Методом  Array.isArray(), який повертає true, якщо об'єкт є масивом
*/
function filterBy(Array, DataType ){
const result = Array.filter(item => typeof item !== DataType)
return result;
};
console.log(filterBy(['hello', 'world', 23, '23', null], "string"));
console.log(filterBy(['hello', 'world', 23, '23', null], "object"));