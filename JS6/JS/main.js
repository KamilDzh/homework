function createNewUser() {
    const firstName = prompt("What is your name?");
    const lastName = prompt("What is your last name?");
    const birthday = getValue('birthday (dd.mm.yyyy)');
  
    const validBirthday = birthday.split('.')
  
    const newUser = {
      firstName: firstName,
      lastName: lastName,
      birthday: birthday,
      getLogin: function () {
        const lowercase = firstName.charAt(0) + lastName;
        const result = lowercase.toLowerCase();
        return result;
      },
      getAge() {
        const nowDate = new Date();
        let correct = 0;
        if (this.birthday.split('.')[1] > nowDate.getMonth() + 1) {
          correct = 1;
        }
        if (this.birthday.split('.')[1] == nowDate.getMonth() + 1) {
          if (this.birthday.split('.')[0] > nowDate.getDate()) {
            correct = 1;
          }
        }
        return nowDate.getFullYear() - this.birthday.split('.')[2] - correct;
      },
      getPassword: function () {
        const pass = (firstName.charAt(0)).toUpperCase() + lastName.toLowerCase() + validBirthday[2];
        return pass;
      }
    }
    return newUser;
  };
  function getValue(text) {
    return prompt(`Enter your ${text}`).trim();
  }
  
  const user = createNewUser()
  console.log(user.getAge());
  console.log(user.getPassword());
  
  