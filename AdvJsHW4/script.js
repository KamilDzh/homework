// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript:
//  Ajax, або Asynchronous Javascript and XML, застосовується в тих випадках, коли необхідно створити асинхронний запит до сервера, тобто, виконати його в фоновому режимі без перезавантаження сторінки








fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(res => res.json())
    .then(movies => {
        movies.forEach(movie => showMovies(movie))
    });


function showMovies(movie) {
    const FilmHolder = document.createElement('div')
    const MovieNameEl = document.createElement('span')
    const episodeIdEl = document.createElement('p')
    const openingCrawlEl = document.createElement('p')
    episodeIdEl.innerText = movie.episodeId
    openingCrawlEl.innerText = movie.openingCrawl
    MovieNameEl.innerText = movie.name;
    const chars = document.createElement('div');
    FilmHolder.append(MovieNameEl)
    FilmHolder.append(episodeIdEl)
    FilmHolder.append(openingCrawlEl)
    FilmHolder.append(chars)
    document.body.append(FilmHolder)

    Promise.all(
        movie.characters.map(char => fetch(char)
            .then(r => r.json())))
        .then(characters => {
            characters.forEach(character => {
                const charactersEl = document.createElement('li');
                charactersEl.innerText = character.name
                chars.append(charactersEl)
            })
        })

}

