/////////////////////////////////////////////////////////////////////////
/*
Тому що ввод данних може бути не тільки через клавіатуру а наприклад й через
голосовий ввід або вставка через кнопки миші
*/

const btnEl = document.querySelectorAll('.btn');
document.addEventListener('keydown', (event) => {
  btnEl.forEach((btn) => {
    btn.classList.remove('btnPress');
    if (event.key === btn.innerText) {
      btn.classList.add('btnPress');
    } else if (event.key === btn.innerText.toLowerCase()) {
      btn.classList.add('btnPress');
    }
  });
});
