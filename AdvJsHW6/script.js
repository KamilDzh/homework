const seacrhBtn = document.querySelector('.button')

seacrhBtn.addEventListener('click', seacrhIp)

async function seacrhIp() {
    try {
        const ipGet = await fetch('https://api.ipify.org/?format=json');
        const getInfo = await ipGet.json();
        console.log(getInfo);
        
        const getLocation = await fetch (`http://ip-api.com/json/${getInfo.ip}?fields=continent,district,country,region,regionName,city`,{method: "GET",})
        const ipresult = await getLocation.json();
        SiteStructure(ipresult);
        console.log(ipresult);
    }catch (error) {
        console.error(error);
    }
}

// fetch('https://api.ipify.org/?format=json').then((r)=> r.json()).then(data=>console.log(data));
// fetch('https://ip-api.com/').then((r)=>r.json()).then(data=>console.log(data))

function SiteStructure(ip){
    const devEl = document.createElement('div');
    const continent = document.createElement('p');
    const country = document.createElement('p');
    const region = document.createElement('p');
    const city = document.createElement('p');
    const district = document.createElement('p');
    continent.innerText= ip.continent;
    country.innerText = ip.country;
    region.innerText = ip.region;
    city.innerText = ip.city;
    district.innerText = ip.district;
    devEl.append(continent)
    devEl.append(country)
    devEl.append(region)
    devEl.append(city)
    devEl.append(district)
    document.body.append(devEl)
}

